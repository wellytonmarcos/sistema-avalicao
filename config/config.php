<?php 
/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| 	[project_folder] 		Pasta em que o o projeto está alocado, caso esteja no localhost, deixe em branco
|	[BASE_URL]				Define o Diretorio base da aplicação
|	[BASE_ABS]				Define a rota absoluta do projeto
|	[DESTINATARIO_DEFAULT]	Define o email que receberá solicitações

*/
$project_folder="projetos/sistema-avalicao/";

define('BASE_URL',	"http://{$_SERVER['HTTP_HOST']}/{$project_folder}"	);

if(substr($_SERVER['DOCUMENT_ROOT'],-1)=='/')
{ 
	define('BASE_ABS',"{$_SERVER['DOCUMENT_ROOT']}{$project_folder}"); 
}
else
{ 
	define('BASE_ABS',"{$_SERVER['DOCUMENT_ROOT']}/{$project_folder}"); 
}
define('DESTINATARIO_DEFAULT', 	'wellytonmarcos@gmail.com');
/*
|--------------------------------------------------------------------------
| Adicional Path URL
|--------------------------------------------------------------------------
|
| 	[BASE_IMG] 				Local onde é armazenado as imagens
| 	[BASE_CSS] 				Local onde é armazenado os CSS
| 	[BASE_JS ] 				Local onde é armazenado os JS
| 	[BASE_PLUGIN] 				Local onde é armazenado so Plugins de 3ºs
*/
define('BASE_IMG',		BASE_URL."public/img/");
define('BASE_CSS',		BASE_URL."public/css/");
define('BASE_JS',		BASE_URL."public/js/");
define('BASE_PLUGIN',	BASE_URL."public/plugins/");

/*
|--------------------------------------------------------------------------
| Database
|--------------------------------------------------------------------------
|
| 	[HOST] 				Host do DB
| 	[DB] 				Nome do Banco
| 	[USER] 				Usuário do Banco
| 	[PASSWORD] 			Senha do Banco
*/
define('HOST', 	"localhost");
define('DB',	"sistema");
define('USER', 	"root");
define('PASSWORD',"");

