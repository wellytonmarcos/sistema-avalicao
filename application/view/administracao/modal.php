				<!-- Modal para editar credores -->
				<div class="modal fade" id="modal_visualizar">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <h5 class="modal-title">Detalhes da Solicitação</h5>
				                <button type="button" class="close" data-dismiss="modal">&times;</button>
				            </div>
			                <div class="modal-body">
			                	<div id="content_visualizar">
			                		
			                	</div>
			                </div>
			                <div class="modal-footer">
			                	<button type="button" class="btn btn-light" data-dismiss="modal" title="Fechar">Fechar</button>
			                </div>				            
				        </div>
				    </div>
				</div>
				<!-- Modal para editar/excluir cliente -->
				<div class="modal fade" id="modal_editar_cliente">
				    <div class="modal-dialog modal-lg">
				        <div class="modal-content">
				            <div class="modal-header">
				                <h5 class="modal-title">Editar Cliente</h5>
				                <button type="button" class="close" data-dismiss="modal">&times;</button>
				            </div>
			                <div class="modal-body">
			                	<div class="row">
			                		<input type="hidden" name="ID_CLIENTE" id="ID_CLIENTE" >
			                		<div class="col-12">
										<div class="form-group">
											<label for="NOME">Nome (<span class="text-danger">*</span>)</label>
											<input type="text"  class="form-control" id="NOME" name="NOME" aria-describedby="NOME_HELP" placeholder="Insira seu nome" maxlength="80">
											<small id="NOME_HELP" class="form-text text-danger"></small>
										</div>
									</div>
									<div class="col-12 col-sm-6">
										<div class="form-group">
											<label for="EMAIL">E-mail (<span class="text-danger">*</span>)</label>
											<input type="email"  class="form-control" id="EMAIL" name="EMAIL" aria-describedby="EMAIL_HELP" placeholder="Insira seu e-mail">
											<small id="EMAIL_HELP" class="form-text text-danger"></small>
										</div>
									</div>	
									<div class="col-12 col-sm-6">
										<div class="form-group">
											<label for="CELULAR">Celular (<span class="text-danger">*</span>)</label>
											<input type="text"  class="form-control" id="CELULAR" name="CELULAR" aria-describedby="CELULAR_HELP" placeholder="Insira seu celular">
											<small id="CELULAR_HELP" class="form-text text-danger"></small>
										</div>
									</div>	
									<div class="col-12 col-sm-8">
										<div class="form-group">
											<label for="RUA">Rua (<span class="text-danger">*</span>)</label>
											<input type="text"  class="form-control" id="RUA" name="RUA" aria-describedby="RUA_HELP" placeholder="Insira a rua onde mora">
											<small id="RUA_HELP" class="form-text text-danger"></small>
										</div>
									</div>	
									<div class="col-12 col-sm-4">
										<div class="form-group">
											<label for="NUMERO">Número (<span class="text-danger">*</span>)</label>
											<input type="text"  class="form-control" id="NUMERO" name="NUMERO" aria-describedby="NUMERO_HELP" placeholder="Insira o número do seu endereço">
											<small id="NUMERO_HELP" class="form-text text-danger"></small>
										</div>
									</div>	
									<div class="col-12 col-sm-4">
										<div class="form-group">
											<label for="BAIRRO">Bairro (<span class="text-danger">*</span>)</label>
											<input type="text"  class="form-control" id="BAIRRO" name="BAIRRO" aria-describedby="BAIRRO_HELP" placeholder="Insira o bairro">
											<small id="BAIRRO_HELP" class="form-text text-danger"></small>
										</div>
									</div>	
									<div class="col-12 col-sm-4">
										<div class="form-group">
											<label for="CIDADE">Cidade (<span class="text-danger">*</span>)</label>
											<input type="text"  class="form-control" id="CIDADE" name="CIDADE" aria-describedby="CIDADE_HELP" placeholder="Insira a Cidade">
											<small id="CIDADE_HELP" class="form-text text-danger"></small>
										</div>
									</div>	
									<div class="col-12 col-sm-4">
										<div class="form-group">
											<label for="CEP">CEP (<span class="text-danger">*</span>)</label>
											<input type="text"  class="form-control" id="CEP" name="CEP" aria-describedby="CEP_HELP" placeholder="Insira o CEP">
											<small id="CEP_HELP" class="form-text text-danger"></small>
										</div>
									</div>	
									<div class="col-12 col-sm-4">
										<div class="form-group">
											<label for="ESTADO">Estado (<span class="text-danger">*</span>)</label>
											<select class="form-control" id="ESTADO" name="ESTADO" aria-describedby="ESTADO_HELP" placeholder="Selecione o Estado">
												<option value="" disabled selected>Selecione o estado</option>
												<option value="AC">Acre</option>
												<option value="AL">Alagoas</option>
												<option value="AP">Amapá</option>
												<option value="AM">Amazonas</option>
												<option value="BA">Bahia</option>
												<option value="CE">Ceará</option>
												<option value="DF">Distrito Federal</option>
												<option value="ES">Espírito Santo</option>
												<option value="GO">Goiás</option>
												<option value="MA">Maranhão</option>
												<option value="MT">Mato Grosso</option>
												<option value="MS">Mato Grosso do Sul</option>
												<option value="MG">Minas Gerais</option>
												<option value="PA">Pará</option>
												<option value="PB">Paraíba</option>
												<option value="PR">Paraná</option>
												<option value="PE">Pernambuco</option>
												<option value="PI">Piauí</option>
												<option value="RJ">Rio de Janeiro</option>
												<option value="RN">Rio Grande do Norte</option>
												<option value="RS">Rio Grande do Sul</option>
												<option value="RO">Rondônia</option>
												<option value="RR">Roraima</option>
												<option value="SC">Santa Catarina</option>
												<option value="SP">São Paulo</option>
												<option value="SE">Sergipe</option>
												<option value="TO">Tocantins</option>
											</select>
											<small id="ESTADO_HELP" class="form-text text-danger"></small>
										</div>
									</div>	
									<div class="col-12 col-sm-4">
										<div class="form-group">
											<label for="SETOR">Setor (<span class="text-danger">*</span>)</label>
											<input type="text"  class="form-control" name="SETOR" id="SETOR" aria-describedby="SETOR_HELP" placeholder="Insira o setor">
											<small id="SETOR_HELP" class="form-text text-danger"></small>
										</div>
									</div>
									<div class="col-12 col-sm-4">
										<div class="form-group">
											<label for="CURSO">Curso (<span class="text-danger">*</span>)</label>
											<input type="text"  class="form-control" id="CURSO" name="CURSO" aria-describedby="CURSO_HELP" placeholder="Insira o curso">
											<small id="CURSO_HELP" class="form-text text-danger"></small>
										</div>
									</div>
			                	</div>
			                	<div class="row">
			                		<div class="col-12">
			                			<p><strong class="text-danger">Atenção!</strong> Excluir em cliente removerá todas as solicitações cadastradas pelo mesmo.</p>
			                		</div>
			                	</div>
			                </div>
			                <div class="modal-footer">
			                	<button type="button" class="btn btn-light" data-dismiss="modal" title="Fechar">Fechar</button>
			                	<button type="button" class="btn btn-danger"  title="Excluir" onclick="excluirCliente()">Excluir</button>
			                	<button type="button" class="btn btn-primary"  title="Atualizar" onclick="atualizarCliente()">Atualizar</button>
			                </div>				            
				        </div>
				    </div>
				</div>
				<!-- Modal para editar solicitação -->
				<div class="modal fade" id="modal_editar_solicitacao">
				    <div class="modal-dialog modal-lg">
				        <div class="modal-content">
				            <div class="modal-header">
				                <h5 class="modal-title">Editar Solicitação</h5>
				                <button type="button" class="close" data-dismiss="modal">&times;</button>
				            </div>
			                <div class="modal-body">
			                	<div class="row">
			                		<input type="hidden" name="ID_SOLICITACAO" id="ID_SOLICITACAO" >
			                		<div class="col-12">
										<div class="form-group">
											<label for="SOLICITACAO">Solicitação (<span class="text-danger">*</span>)</label>
											<textarea class="form-control" id="SOLICITACAO" name="SOLICITACAO" aria-describedby="SOLICITACAO_HELP" placeholder="Insira sua solicitação"></textarea>
											<small id="SOLICITACAO_HELP" class="form-text text-danger"></small>
										</div>
									</div>
			                	</div>
			                </div>
			                <div class="modal-footer">
			                	<button type="button" class="btn btn-light" data-dismiss="modal" title="Fechar">Fechar</button>
			                	<button type="button" class="btn btn-primary"  title="Atualizar" onclick="atualizarSolicitacao()">Atualizar</button>
			                </div>				            
				        </div>
				    </div>
				</div>
				<!-- Modal para editar solicitação -->
				<div class="modal fade" id="modal_excluir_solicitacao">
				    <div class="modal-dialog modal-lg">
				        <div class="modal-content">
				            <div class="modal-header">
				                <h5 class="modal-title">Excluir Solicitação</h5>
				                <button type="button" class="close" data-dismiss="modal">&times;</button>
				            </div>
			                <div class="modal-body">
			                	<div class="row">
			                		<div class="col-12">
			                			<p>Tem certeza que deseja excluir esta solicitação?</p>
			                		</div>
			                	</div>
			                </div>
			                <div class="modal-footer">
			                	<button type="button" class="btn btn-light" data-dismiss="modal" title="Cancelar">Cancelar</button>
			                	<button type="button" class="btn btn-danger" id="btn_excluir"  title="Excluir">Excluir</button>
			                </div>				            
				        </div>
				    </div>
				</div>