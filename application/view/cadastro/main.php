<div class="container" >
	<form name="form_cadastro" id="form_cadastro" action="<?php echo BASE_URL.'cadastro/cadastrar'; ?>" method="post">
		<div class="row">
			<div class="col-12 my-4">
				<h3>Cadastrar Solicitação</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="TIPO">Tipo de Cliente (<span class="text-danger">*</span>)</label>
					<select class="form-control" id="TIPO" name="TIPO" aria-describedby="TIPO_HELP" placeholder="Selecione o Tipo de Solicitante" onchange="defineTipo();">
						<option value="" disabled selected>Selecione um tipo</option>
						<option value="Aluno">Aluno</option>
						<option value="Professor">Professor</option>
						<option value="Técnico Administrativo">Técnico Administrativo</option>
					</select>
					<small id="TIPO_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="CODIGO">Código (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" id="CODIGO" name="CODIGO" aria-describedby="CODIGO_HELP" placeholder="Insira seu código" disabled onkeyup="verificaExiste()">
					<small id="CODIGO_HELP" class="form-text text-danger"></small>
				</div>
			</div>
			<input type="hidden" name="OPERACAO" id="OPERACAO" value="">
		</div>
		<div class="row" id="demais_campos" style="display: none;">
			
			<div class="col-12">
				<div class="form-group">
					<label for="NOME">Nome (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" id="NOME" name="NOME" aria-describedby="NOME_HELP" placeholder="Insira seu nome" maxlength="80">
					<small id="NOME_HELP" class="form-text text-danger"></small>
				</div>
			</div>
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="EMAIL">E-mail (<span class="text-danger">*</span>)</label>
					<input type="email"  class="form-control" id="EMAIL" name="EMAIL" aria-describedby="EMAIL_HELP" placeholder="Insira seu e-mail">
					<small id="EMAIL_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12 col-sm-6">
				<div class="form-group">
					<label for="CELULAR">Celular (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" id="CELULAR" name="CELULAR" aria-describedby="CELULAR_HELP" placeholder="Insira seu celular">
					<small id="CELULAR_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12 col-sm-8">
				<div class="form-group">
					<label for="RUA">Rua (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" id="RUA" name="RUA" aria-describedby="RUA_HELP" placeholder="Insira a rua onde mora">
					<small id="RUA_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12 col-sm-4">
				<div class="form-group">
					<label for="NUMERO">Número (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" id="NUMERO" name="NUMERO" aria-describedby="NUMERO_HELP" placeholder="Insira o número do seu endereço">
					<small id="NUMERO_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12 col-sm-4">
				<div class="form-group">
					<label for="BAIRRO">Bairro (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" id="BAIRRO" name="BAIRRO" aria-describedby="BAIRRO_HELP" placeholder="Insira o bairro">
					<small id="BAIRRO_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12 col-sm-4">
				<div class="form-group">
					<label for="CIDADE">Cidade (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" id="CIDADE" name="CIDADE" aria-describedby="CIDADE_HELP" placeholder="Insira a Cidade">
					<small id="CIDADE_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12 col-sm-4">
				<div class="form-group">
					<label for="CEP">CEP (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" id="CEP" name="CEP" aria-describedby="CEP_HELP" placeholder="Insira o CEP">
					<small id="CEP_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12 col-sm-4">
				<div class="form-group">
					<label for="ESTADO">Estado (<span class="text-danger">*</span>)</label>
					<select class="form-control" id="ESTADO" name="ESTADO" aria-describedby="ESTADO_HELP" placeholder="Selecione o Estado">
						<option value="" disabled selected>Selecione o estado</option>
						<option value="AC">Acre</option>
						<option value="AL">Alagoas</option>
						<option value="AP">Amapá</option>
						<option value="AM">Amazonas</option>
						<option value="BA">Bahia</option>
						<option value="CE">Ceará</option>
						<option value="DF">Distrito Federal</option>
						<option value="ES">Espírito Santo</option>
						<option value="GO">Goiás</option>
						<option value="MA">Maranhão</option>
						<option value="MT">Mato Grosso</option>
						<option value="MS">Mato Grosso do Sul</option>
						<option value="MG">Minas Gerais</option>
						<option value="PA">Pará</option>
						<option value="PB">Paraíba</option>
						<option value="PR">Paraná</option>
						<option value="PE">Pernambuco</option>
						<option value="PI">Piauí</option>
						<option value="RJ">Rio de Janeiro</option>
						<option value="RN">Rio Grande do Norte</option>
						<option value="RS">Rio Grande do Sul</option>
						<option value="RO">Rondônia</option>
						<option value="RR">Roraima</option>
						<option value="SC">Santa Catarina</option>
						<option value="SP">São Paulo</option>
						<option value="SE">Sergipe</option>
						<option value="TO">Tocantins</option>
					</select>
					<small id="ESTADO_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12 col-sm-4">
				<div class="form-group">
					<label for="SETOR">Setor (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" name="SETOR" id="SETOR" aria-describedby="SETOR_HELP" placeholder="Insira o setor">
					<small id="SETOR_HELP" class="form-text text-danger"></small>
				</div>
			</div>
			<div class="col-12 col-sm-4">
				<div class="form-group">
					<label for="CURSO">Curso (<span class="text-danger">*</span>)</label>
					<input type="text"  class="form-control" id="CURSO" name="CURSO" aria-describedby="CURSO_HELP" placeholder="Insira o curso">
					<small id="CURSO_HELP" class="form-text text-danger"></small>
				</div>
			</div>	
			<div class="col-12">
				<div class="form-group">
					<label for="SOLICITACAO">Solicitação (<span class="text-danger">*</span>)</label>
					<textarea class="form-control" id="SOLICITACAO" name="SOLICITACAO" aria-describedby="SOLICITACAO_HELP" placeholder="Insira sua solicitação"></textarea>
					<small id="SOLICITACAO_HELP" class="form-text text-danger"></small>
				</div>
			</div>
			<div class="col-12">
				<div class="form-group text-right my-2">
			<button type="button" class="btn btn-primary" onclick="enviarCadastro()">Cadastrar Solicitação</button>
		</div>
			</div>
		</div>
				
	</form>
</div>
