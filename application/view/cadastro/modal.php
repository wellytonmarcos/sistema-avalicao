				<!-- Modal para editar credores -->
				<div class="modal fade" id="modal_sucesso">
				    <div class="modal-dialog">
				        <div class="modal-content">
				            <div class="modal-header">
				                <h5 class="modal-title">Solicitação Enviada</h5>
				                <button type="button" class="close" data-dismiss="modal">&times;</button>
				            </div>
			                <div class="modal-body">
			                	<p>Sua solicitação foi enviada com sucesso! O setor de TI já está acompanhando sua demanda e em breve entrará em contato pelo telefone: <span id="TELEFONE_CADASTRADO"></span> ou pelo e-mail: <span id="EMAIL_CADASTRADO"></span></p>
			                </div>
			                <div class="modal-footer">
			                	<button type="button" class="btn btn-light" data-dismiss="modal" title="Cancelar">Nova Solicitação</button>
			                    <a href="<?php echo BASE_URL; ?>" class="btn btn-primary" name="credor" value="Atualizar" title="Atualizar Dados">Ir Para Página Inicial</a>
			                </div>				            
				        </div>
				    </div>
				</div>