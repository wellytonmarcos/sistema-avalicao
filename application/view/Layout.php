<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="Avaliação, Unilavras, Teste" name="keywords">
    <meta content="Teste de Conhecimento em PHP" name="description">
    <title><?php echo $this->get_title(); ?></title>
    <!-- Base URL -->
    <script> var base_url = "<?php echo BASE_URL; ?>";</script>
    <!-- Stylesheet -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="<?php echo BASE_CSS.'bootstrap.min.css' ?>" rel="stylesheet">
    <script src="<?php echo BASE_JS.'jquery.min.js' ?>" type="text/javascript"></script>
    <script src="<?php echo BASE_JS.'bootstrap.min.js' ?>" type="text/javascript"></script>
    <?php echo $this->add_head(); ?>
</head>
<body>
    <section id="add-nav">
	    <nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
	    <a class="navbar-brand" href="<?php echo BASE_URL; ?>">Sistema de Solicitações do TI</a>
	    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="navbar-toggler-icon"></span>
	    </button>

	    <div class="collapse navbar-collapse text-right" id="nav-content">
	        <ul class="navbar-nav mr-auto"></ul>
	        <ul class="navbar-nav mr-0">
	            <li class="nav-item">
	                <a class="nav-link" href="<?php echo BASE_URL; ?>">Home</a>
	            </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo BASE_URL; ?>cadastro">Nova Solicitação</a>
                </li>
	            <li class="nav-item">
	                <a class="nav-link" href="<?php echo BASE_URL; ?>administracao">Administração</a>
	            </li>
	            
	        </ul>
	    </div>
	</nav>
    </section>
    <section id="add-nav">
    <?php echo $this->add_modal(); ?>
    </section>
    <section id="add-main">
        <?php echo $this->add_main(); ?>
    </section>
</body>

</html>