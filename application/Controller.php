<?php

namespace App;

use Src\Classes\Routes;

class Controller extends Routes{

    #Atributos
    private $method;
    private $param=[];
    private $obj;

    #Método Construtor
    public function __construct()
    {
       self::add_controller();
    }

    protected function get_method() { return $this->method; } //Retorna Metodo
    public function set_method($method) { $this->method = $method; }//Seta Metodo
    protected function get_param() { return $this->param; } //Retorna Parametro
    public function set_param($param) { $this->param = $param; }//Seta Parametro
    #Método de adição de controller
    private function add_controller()
    {
        
        $my_controller = $this->get_route(); //Recebe a rota
        $controller_name = "App\\Controller\\{$my_controller}"; //Monta o patch
        $this->obj = new $controller_name; //Monta obj

        if(isset($this->parse_url()[1]))
        { //Verifica se existe a posição 1 = metodo
            self::add_method(); //Chama metodo 
        }
    }

    #Método de adição de método do controller
    private function add_method()
    {
        if(method_exists($this->obj, $this->parse_url()[1]))
        { //Se existir o metodo no objeto
            $this->set_method("{$this->parse_url()[1]}"); //Seta o metodo
            self::add_param(); //Chama parametro
            call_user_func_array([$this->obj,$this->get_method()],$this->get_param()); 
        }
    }

    #Método de adição de parâmetros do controller
    private function add_param()
    {
        $array_url = $this->parse_url(); //Obtem array url
        $numero = count($array_url); //Conta tamanho URL
        if($numero > 2) //Se o tamanho for maior que 2, existe parametro
        {
            foreach ($array_url as $indice => $item) //percorre array
            {
                if($indice > 1){ //Se for maior que 1 (pos 0 e 1)
                    $this->set_param($this->param += [$indice => $item]); //Seta paramentro
                }
            }
        }
    }
}