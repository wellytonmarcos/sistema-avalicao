<?php
namespace App\Controller;
use Src\Classes\Render; //Classe de Renderização
use App\Model\MGlobal; //Model Global
use Src\Traits\Conversor; //Trait Conversor 

/**
 * Classe de Controle da Tela de Administração
 */
class Administracao extends MGlobal {

    use Conversor; //Usa metodos do conversor	
    /**
     * Contrutor da Classe, renderiza quando não há requisição POST
     */
    public function __construct()
    {
    	if(empty($_POST)) //Se não existir POST
    	{
    		$render = new Render(); //Chama a render
	        $render->set_title("Administrar Solicitação"); //Passa o titulo
	        $render->set_dir("administracao"); //Passa o diretorio
	        $render->render_layout(); //Renderiza layout
    	}    	
    }

    /* METODOS RELACIONADOS A SOLICITAÇÃO */
    /**
     * Metodo de Listar solicitações
     * @return [view] [Renderiza código HTML/PHP]
     */
    public function listar()
    {
        $solicitacoes = $this->read_solicitacao(); //Busca todas as solicitações no Banco de Dados     
            
        if(is_array($solicitacoes)) //Se encontrar
        {
            //Monta uma tabela com dados (deve ser melhorado escrevendo isso na view)
            $tabela =  '
                <table class="table">
                    <thead>
                        <tr>
                            <th>Identificador</th>
                            <th>Código/Solicitante</th>
                            <th>Tipo</th>
                            <th>Telefone/E-mail</th>
                            <th>Data</th>
                            <th width="200">Ações</th>        
                        </tr>
                    </thead>
                <tbody>';
            foreach ($solicitacoes as $s) 
            {
                $tabela .= '
                    <tr>
                        <td>'.$s['ID_SOLICITACAO'].'</td>
                        <td>'.$s['CODIGO'].'<br>'.$s['NOME'].'</td>
                        <td>'.$s['TIPO'].'</td>
                        <td>'.$s['CELULAR'].'<br>'.$s['EMAIL'].'</td>
                        <td>'.$this->converter_datahora_brasil($s['DATA']).'</td>
                        <td>
                            <img src="'.BASE_IMG.'ver.png" class="btn btn-link p-0" width="30" alt="Visualizar" title="Visualizar" onclick="visualizarSolicitacao('.$s['ID_SOLICITACAO'].')">
                            <img src="'.BASE_IMG.'editar.png" class="btn btn-link p-0" width="30" alt="Editar Solicitação" title="Editar Solicitação" onclick="editarSolicitacao('.$s['ID_SOLICITACAO'].')">
                            <img src="'.BASE_IMG.'cliente.png" class="btn btn-link p-0" width="30" alt="Editar Cliente" title="Editar Cliente" onclick="editarCliente('.$s['CODIGO'].')">
                            <img src="'.BASE_IMG.'deletar.png" class="btn btn-link p-0" width="30" alt="Excluir" title="Excluir" onclick="confirmaExcluirSolicitacao('.$s['ID_SOLICITACAO'].')">
                        </td>
                    </tr>
                    ';
            }
            $tabela .= '
                    </tbody>
                </table>';
            echo $tabela; //Echo na Tabela montada
        }
        else
        {
            echo '<h4>Nenhuma Solicitação Cadastrada!</h4>'; //Renderiza que não há solicitações
        }
    }
    /**
     * Metodo de Visualizar uma Solicitacao
     * @return [view] [Renderiza código HTML/PHP]
     */
    public function visualizar($ID_SOLICITACAO)
    {
        $solicitacao = $this->read_solicitacao($ID_SOLICITACAO); //Busca a solicitações no Banco de Dados  
        if(is_array($solicitacao)) //Se retornar
        {
            $s = $solicitacao[0]; //Passa a solicitação para a posição

            $dados = '
                <p>Identificador: '.$s['ID_SOLICITACAO'].'</p>
                <p>Tipo: '.$s['TIPO'].'</p>
                <p>Código: '.$s['CODIGO'].'</p>
                <p>Nome: '.$s['NOME'].'</p>
                <p>Celular: '.$s['CELULAR'].'</p>
                <p>E-mail: '.$s['EMAIL'].'</p>
                <p>Endereço: '.$s['RUA'].', '.$s['NUMERO'].' - '.$s['BAIRRO'].'</p>
                <p>Cidade: '.$s['CIDADE'].'</p>
                <p>CEP: '.$s['CEP'].'</p>
                <p>Estado: '.$s['ESTADO'].'</p>
                <p>Setor: '.$s['SETOR'].'</p>
                <p>Curso: '.$s['CURSO'].'</p>
                <p>Solicitação: '.$s['SOLICITACAO'].'</p>
                <p>Data: '.$this->converter_datahora_brasil($s['DATA']).'</p>                
            ';
            echo $dados;

        }
        else
        {
            echo '<p>Solicitação não encontrada!</p>';
        }
    } 
    /**
     * Metodo que retorna uma Solicitacao
     * @return [json] [JSON com a Solicitação]
     */
    public function buscar_solicitacao($ID_SOLICITACAO)
    {
        $solicitacao = $this->read_solicitacao($ID_SOLICITACAO); //Busca a solicitações no Banco de Dados  
        if(is_array($solicitacao)) //Se retornar
        {
            echo json_encode($solicitacao[0]['SOLICITACAO']); //Passa a solicitação para a posição
        }
        else
        {
            echo false;
        }
    }
    /**
     * Metodo que atualiza uma solicitacao
     * @return [json] [Renderiza código HTML/PHP]
     */
    public function atualizar_solicitacao($ID_SOLICITACAO)
    {
        $solicitacao = $this->update_solicitacao($ID_SOLICITACAO, $_POST['SOLICITACAO']); //Atualiza solicitacao  
        if($solicitacao) //Se retornar
        {
            echo json_encode(true); //retorna true
        }
        else
        {
            echo json_encode(false); //retorna false
        }
    }
    /**
     * [deletar_solicitacao - Deleta uma solicitação]
     * @return [json] [Operação]
     */
    public function deletar_solicitacao($ID_SOLICITACAO)
    {
        $resultado = $this->delete_solicitacao($ID_SOLICITACAO); //Deletar solicitação
        if($resultado) //Se deu certo
        {
            $retorno = [ //Caso de sucesso
                'result' => true,
            ];
            echo json_encode($retorno);
        }
        else
        {
            $retorno = [
                'result' => false,
                'error' => 'Falha ao cadastrar solicitação!',
            ];
            echo json_encode($retorno); //Retorna 
        }
    } 
    /* OPERAÇÕES COM CLIENTE*/
    /**
     * [atualizar - Metodo de Atualizar de Cliente]
     * @return [json] [Operação]
     */
    public function atualizar_cliente($ID_CLIENTE)
    {        
        $resultado = $this->update_cliente($ID_CLIENTE, $_POST['NOME'], $_POST['CELULAR'], $_POST['EMAIL'], $_POST['RUA'], $_POST['NUMERO'], $_POST['BAIRRO'], $_POST['CIDADE'], $_POST['CEP'], $_POST['ESTADO'], $_POST['SETOR'], $_POST['CURSO']); //Atualiza Cliente

        if($resultado) //Se deu certo
        {
            $retorno = [ //Caso de sucesso
                'result' => true,
            ];
            echo json_encode($retorno);
        }
        else
        {
            $retorno = [
                'result' => false,
                'error' => 'Falha ao cadastrar solicitação!',
            ];
            echo json_encode($retorno); //Retorna 
        }
    } 
    /**
     * [deletar_cliente - Remove Cliente e Todas suas Solicitações]
     * @return [json] [Operação]
     */
    public function deletar_cliente($ID_CLIENTE)
    {
        $resultado = $this->delete_cliente($ID_CLIENTE); //Deletar Cliente
        if($resultado) //Se deu certo
        {
            $retorno = [ //Caso de sucesso
                'result' => true,
            ];
            echo json_encode($retorno);
        }
        else
        {
            $retorno = [
                'result' => false,
                'error' => 'Falha ao cadastrar solicitação!',
            ];
            echo json_encode($retorno); //Retorna 
        }
    } 
      
}
