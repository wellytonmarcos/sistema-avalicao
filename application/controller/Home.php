<?php
namespace App\Controller;
use Src\Classes\Render;
use Src\Interfaces\View;

class Home extends Render {
    public function __construct()
    {
        $this->set_title("Página Inicial");
        $this->set_dir("home");
        $this->render_layout();
    }
}