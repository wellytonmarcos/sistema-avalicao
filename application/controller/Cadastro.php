<?php
namespace App\Controller;
use Src\Classes\Render;
use App\Model\MGlobal;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
/**
 * Classe de Cadastro do por parte do cliente
 */
class Cadastro extends MGlobal {

	private $CODIGO, $NOME, $CELULAR, $EMAIL, $RUA, $NUMERO, $BAIRRO, $CIDADE, $CEP, $ESTADO, $TIPO, $SETOR, $CURSO, $SOLICITACAO; //Variáveis Privadas
    /**
     * Contrutor da Classe, renderiza quando não há requisição POST
     */
    public function __construct()
    {
    	if(empty($_POST)) //Se não existir POST
    	{
    		$render = new Render(); //Cria uma Render
	        $render->set_title("Cadastrar Solicitação"); //Seta o titulo
	        $render->set_dir("cadastro"); //Define diretório
	        $render->render_layout(); //Renderiza tela

    	}    	
    }
    /**
     * Validação do POST em PHP
     */
    private function valida_post()
    {
    	$erro_validacao = false; //Variável Boleana de Validação
    	$validacao_input = []; //Mensagem validação
    	/* Validação do input TIPO */
    	if(isset($_POST['TIPO']) && $_POST['TIPO'] != '') //Tipo deve ser preenchido
		{
			$this->TIPO = filter_input(INPUT_POST, 'TIPO', FILTER_SANITIZE_SPECIAL_CHARS); //Atribui tipo
			$validacao_input['TIPO'] = '';
		}
		else
		{
			$erro_validacao = true; 
			$validacao_input['TIPO'] = 'O tipo é obrigatório'; //Erro
		}
		/* Validação do input CÓDIGO */
    	if(isset($_POST['CODIGO']))
    	{ 
    		if(isset($_POST['TIPO']) && $_POST['TIPO'] != '') //Valida se o tipo foi selecionado
    		{
    			if($_POST['TIPO'] == 'Aluno') //Se for aluno
    			{
    				if(strlen($_POST['CODIGO']) == 7) //Deve possuir 7 caracteres
    				{
    					$this->CODIGO = filter_input(INPUT_POST, 'CODIGO', FILTER_SANITIZE_SPECIAL_CHARS);  //Atribui
    					$validacao_input['CODIGO'] = '';
    				}
    				else
    				{
    					$erro_validacao = true;
    					$validacao_input['CODIGO'] = 'O código do aluno deve possuir 7 carateres alfanuméricos'; //erro
    				}
    			}
    			else if($_POST['TIPO'] == 'Professor') //Se for professor
    			{ 
    				if(strlen($_POST['CODIGO']) == 4) //Deve possuir 4 caracteres
    				{
    					$this->CODIGO = filter_input(INPUT_POST, 'CODIGO', FILTER_SANITIZE_SPECIAL_CHARS);  //Atribui
    					$validacao_input['CODIGO'] = '';
    				}
    				else
    				{
    					$erro_validacao = true;
    					$validacao_input['CODIGO'] = 'O código do professor deve possuir 4 carateres alfanuméricos';//Erro
    				}
    			}
    			else if($_POST['TIPO'] == 'Técnico Administrativo') //Se for técnico
    			{
    				if(strlen($_POST['CODIGO']) == 6) //deve possuir 6 caracteres
    				{
    					$this->CODIGO = filter_input(INPUT_POST, 'CODIGO', FILTER_SANITIZE_SPECIAL_CHARS);  //Atribui
    					$validacao_input['CODIGO'] = ''; 
    				} 
    				else
    				{
    					$erro_validacao = true;
    					$validacao_input['CODIGO'] = 'O código do técnico administrativo deve possuir 4 carateres alfanuméricos';//Erro
    				}
    			}
    			else
    			{
    				$erro_validacao = true;
    				$validacao_input['CODIGO'] = 'Selecione um tipo de cliente válido'; //Quando o valor do Tipo não é válido
    			}
    		}
    		else
    		{
    			$erro_validacao = true;
    			$validacao_input['CODIGO'] = 'Selecione um tipo de cliente primeiro'; //Quando o tipo não está selecionado
    		}
    	}
    	else
    	{
    		$erro_validacao = true;
    		$validacao_input['CODIGO'] = 'Código não informado'; //Quando o código não é selecionado
    	}
    	/* Validação do input NOME */
    	if(isset($_POST['NOME']) && $_POST['NOME'] != '')
    	{ 
    		$this->NOME = filter_input(INPUT_POST, 'NOME', FILTER_SANITIZE_SPECIAL_CHARS);  //Atribui
    		$validacao_input['NOME'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['NOME'] = 'O Nome é obrigatório'; //Erro
		}
		/* Validação do input EMAIL */
		if(isset($_POST['EMAIL']) && $_POST['EMAIL'] != '') //E-mail
    	{ 
    		if (filter_var($_POST['EMAIL'], FILTER_VALIDATE_EMAIL))  //Valida se é e-mail
    		{
			   $this->EMAIL = filter_input(INPUT_POST, 'EMAIL', FILTER_SANITIZE_SPECIAL_CHARS); //Atribui
			   $validacao_input['EMAIL'] = '';
			}
			else 
			{
			    $erro_validacao = true;
				$validacao_input['EMAIL'] = 'O e-mail inserido é inválido'; //Erro por não ser email
			}
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['EMAIL'] = 'O e-mail é obrigatório'; //campo em branco
		}
		/* Validação do input CELULAR */
		if(isset($_POST['CELULAR']) && strlen($_POST['CELULAR']) == 14) //Se tiver 14 carateres
    	{ 
    		$this->CELULAR = filter_input(INPUT_POST, 'CELULAR', FILTER_SANITIZE_SPECIAL_CHARS); //Valida campo
    		$validacao_input['CELULAR'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['CELULAR'] = 'O Celular é obrigatório'; //Erro
		}
		/* Validação do input RUA */
		if(isset($_POST['RUA']) && $_POST['RUA'] != '') //Se estiver preenchido
    	{ 
    		$this->RUA = filter_input(INPUT_POST, 'RUA', FILTER_SANITIZE_SPECIAL_CHARS); //Atribui
    		$validacao_input['RUA'] = ''; 
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['RUA'] = 'Rua é obrigatório';
		}
		/* Validação do input NUMERO */
		if(isset($_POST['NUMERO']) && $_POST['NUMERO'] != '')
    	{ 
    		$this->NUMERO = filter_input(INPUT_POST, 'NUMERO', FILTER_SANITIZE_SPECIAL_CHARS); 
    		$validacao_input['NUMERO'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['NUMERO'] = 'Número é obrigatório';
		}
		/* Validação do input BAIRRO */
		if(isset($_POST['BAIRRO']) && $_POST['BAIRRO'] != '')
    	{ 
    		$this->BAIRRO = filter_input(INPUT_POST, 'BAIRRO', FILTER_SANITIZE_SPECIAL_CHARS); 
    		$validacao_input['BAIRRO'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['BAIRRO'] = 'Bairro é obrigatório';
		}
		/* Validação do input CIDADE */
		if(isset($_POST['CIDADE']) && $_POST['CIDADE'] != '')
    	{ 
    		$this->CIDADE = filter_input(INPUT_POST, 'CIDADE', FILTER_DEFAULT); 
    		$validacao_input['CIDADE'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['CIDADE'] = 'Cidade é obrigatório';
		}
		/* Validação do input CIDADE */
		if(isset($_POST['ESTADO']) && strlen($_POST['ESTADO']) == 2)
    	{ 
    		$this->ESTADO = filter_input(INPUT_POST, 'ESTADO', FILTER_SANITIZE_SPECIAL_CHARS); 
    		$validacao_input['ESTADO'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['ESTADO'] = 'Estado é obrigatório';
		}
    	/* Validação do input CEP */
    	if(isset($_POST['CEP']) && strlen($_POST['CEP']) == 9)
    	{ 
    		$this->CEP = filter_input(INPUT_POST, 'CEP', FILTER_SANITIZE_SPECIAL_CHARS); 
    		$validacao_input['CEP'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['CEP'] = 'O CEP é obrigatório';
		}
    	/* Validação do input SETOR */
		if(isset($_POST['SETOR']) && $_POST['SETOR'] != '')
    	{ 
    		$this->SETOR = filter_input(INPUT_POST, 'SETOR', FILTER_SANITIZE_SPECIAL_CHARS); 
    		$validacao_input['SETOR'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['SETOR'] = 'Setor é obrigatório';
		}
		/* Validação do input CURSO */
		if(isset($_POST['CURSO']) && $_POST['CURSO'] != '')
    	{ 
    		$this->CURSO = filter_input(INPUT_POST, 'CURSO', FILTER_SANITIZE_SPECIAL_CHARS); 
    		$validacao_input['CURSO'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['CURSO'] = 'Curso é obrigatório';
		}

		/* Validação do input SOLICITACAO */
		if(isset($_POST['SOLICITACAO']) && $_POST['SOLICITACAO'] != '')
    	{ 
    		$this->SOLICITACAO = filter_input(INPUT_POST, 'SOLICITACAO', FILTER_SANITIZE_SPECIAL_CHARS); 
    		$validacao_input['SOLICITACAO'] = '';
		}
		else
		{
			$erro_validacao = true;
			$validacao_input['SOLICITACAO'] = 'Solicitação é obrigatório';
		}

		if($erro_validacao)
		{
			$retorno = [
				'result' => false,
				'message' => $validacao_input
			];
			return $retorno;
		}
		else
		{
			$retorno = [
				'result' => true,
				'message' => ''
			];
			return $retorno;
		}
    }
    /**
     * [cadastrar - Metodo de Cadastro de Cliente/Solicitação]
     * @return [json] [Operação]
     */
    public function cadastrar()
    {
    	$retorno = $this->valida_post(); //Chama a validação dos dados
    	if($retorno['result']) //Se validou
    	{  
            //Chama o Método de Cadastrar Cliente
    		$cliente = $this->create_cliente($this->CODIGO, $this->NOME, $this->CELULAR, $this->EMAIL, $this->RUA, $this->NUMERO, $this->BAIRRO, $this->CIDADE, $this->CEP, $this->ESTADO, $this->TIPO, $this->SETOR, $this->CURSO);
            //Se a operação for TRUE
    		if($cliente['operacao'])
    		{
                //Cadastra solicitação
    			if($SOLICITACAO = $this->create_solicitacao($cliente['id'],  $this->SOLICITACAO))
                {
                    $this->enviar_email();
                    $retorno = [ //Caso de sucesso
                        'result' => true,
                    ];
                    echo json_encode($retorno);
                }
                else
                {
                    $this->delete_cliente($cliente['id']); //deleta cliente, caso não consiga cadastrar solicitação
                    $retorno = [
                        'result' => false,
                        'error' => 'Falha ao cadastrar cliente!',
                    ];
                    echo json_encode($retorno); //Retorna 
                }
    		}
    		else
            {
                $retorno = [
                    'result' => false,
                    'error' => 'Falha ao cadastrar cliente!',
                ];
                echo json_encode($retorno); //Retorna
            }        	
    	}
        else
        {
        	echo json_encode($retorno);
        }
    }
    /**
     * [atualizar - Metodo de Atualizar de Cliente e Cadastrar Solicitação]
     * @return [json] [Operação]
     */
    public function atualizar()
    {
    	$retorno = $this->valida_post(); //Chama a validação dos dados
    	if($retorno['result']) //Se validou
    	{
    		$this->CODIGO = filter_input(INPUT_POST, 'CODIGO', FILTER_SANITIZE_SPECIAL_CHARS);  //Seta o Código
    		$ID = $this->read_cliente($this->CODIGO); //Busca o cliente
    		
    		if(is_array($ID)) //Se for um array
	    	{
	    		$resultado = $this->update_cliente($ID[0]['ID_CLIENTE'], $this->NOME, $this->CELULAR, $this->EMAIL, $this->RUA, $this->NUMERO, $this->BAIRRO, $this->CIDADE, $this->CEP, $this->ESTADO, $this->SETOR, $this->CURSO); //Atualiza Cliente
	    		if($resultado) //Se deu certo
	    		{
	    			if($SOLICITACAO = $this->create_solicitacao($ID[0]['ID_CLIENTE'],  $this->SOLICITACAO)) //Cadastra solicitação
                    {
                        $this->enviar_email();
                        $retorno = [ //Caso de sucesso
                            'result' => true,
                        ];
                        echo json_encode($retorno);
                    }
                    else
                    {
                        $retorno = [
                            'result' => false,
                            'error' => 'Falha ao cadastrar solicitação!',
                        ];
                        echo json_encode($retorno); //Retorna 
                    }
	    		}
                else
                {
                    $retorno = [
                        'result' => false,
                        'error' => 'Falha ao cadastrar solicitação!',
                    ];
                    echo json_encode($retorno); //Retorna 
                }
	    	}
	    	else
	    	{
	    		echo json_encode(false);
	    	}
    	}
        else
        {
        	echo json_encode($retorno);
        }
    }
    public function buscar()
    {
    	$this->CODIGO = filter_input(INPUT_POST, 'CODIGO', FILTER_SANITIZE_SPECIAL_CHARS); 
    	$cliente = $this->read_cliente($this->CODIGO);
    	if(is_array($cliente))
    	{
    		echo json_encode($cliente[0]);
    	}
    	else
    	{
    		echo json_encode(false);
    	}
    }

    public function enviar_email()
    {

        $mensagem = 'Código: '.$this->CODIGO.'
                    <br>Nome:'.$this->NOME.'
                    <br>Celular: '.$this->CELULAR.'
                    <br>E-mail: '.$this->EMAIL.'
                    <br>Rua: '.$this->RUA.'
                    <br>Número: '.$this->NUMERO.'
                    <br>Bairro: '.$this->BAIRRO.'
                    <br>Cidade: '.$this->CIDADE.'
                    <br>CEP: '.$this->CEP.'
                    <br>Estado: '.$this->ESTADO.'
                    <br>Tipo: '.$this->TIPO.'
                    <br>Setor: '.$this->SETOR.'
                    <br>Curso: '.$this->CURSO.'
                    <br>Solicitação: '.$this->SOLICITACAO;
        //PHPMailer Object
        $mail = new PHPMailer;
        //From
        $mail->From = $this->NOME;
        $mail->FromName = $this->EMAIL;
        //To
        $mail->addAddress(DESTINATARIO_DEFAULT); 
        //Send HTML
        $mail->isHTML(true);
        $mail->Subject = "Nova Solicitação";
        $mail->Body = "Uma nova solicitação foi criada:".$mensagem;
        if(!$mail->send()) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }
}