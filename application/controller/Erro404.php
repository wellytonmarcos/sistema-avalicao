<?php
namespace App\Controller;
use Src\Classes\Render;

class Erro404 extends Render {
    
    public function __construct()
    {
        $this->set_title("Erro 404");
        $this->set_dir("erro404");
        $this->render_layout();
    }
}