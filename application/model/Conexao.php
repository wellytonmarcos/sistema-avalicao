<?php 
namespace App\Model;

abstract class Conexao{
	
	protected $conn;

	public function conectar()
	{
		try
		{
			$this->conn = new \PDO("mysql:host=".HOST.";dbname=".DB."","".USER."","".PASSWORD."");
			return $this->conn;
		}
		catch(\PDOExeption $erro)
		{
			return $erro->getMessage();
		}
	}
	public function desconectar()
	{
		$this->conn = null;
	}
}