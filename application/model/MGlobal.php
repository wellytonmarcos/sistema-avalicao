<?php 
namespace App\Model;

use App\Model\Conexao;

/**
 * Classe de Gerenciamento da Model de Cliente
 */
class MGlobal extends Conexao
{
	private $db;

    /*OPERAÇÕES COM CLIENTE */

    /**
     * Metodo de Cadastro de Clientes 
     */
    protected function create_cliente($CODIGO, $NOME, $CELULAR, $EMAIL, $RUA, $NUMERO, $BAIRRO, $CIDADE, $CEP, $ESTADO, $TIPO, $SETOR, $CURSO)
    {
        //MODELO INSERINDO POR BIND

        $ID_CLIENTE = null; //AUTO INCREMENT
        //$ID_INSERT_CLIENTE = 0;
        $con = $this->conectar();
        try { 
            $this->db = $con->prepare("INSERT INTO cliente VALUES  (:ID_CLIENTE, :CODIGO, :NOME, :CELULAR, :EMAIL, :RUA, :NUMERO, :BAIRRO, :CIDADE, :CEP, :ESTADO, :TIPO, :SETOR, :CURSO)"); //PREPARA INSERÇÃO
            //BIND PARAMETROS
            $this->db->bindParam(":ID_CLIENTE", $ID_CLIENTE, \PDO::PARAM_INT);
            $this->db->bindParam(":CODIGO", $CODIGO, \PDO::PARAM_STR);
            $this->db->bindParam(":NOME", $NOME, \PDO::PARAM_STR);
            $this->db->bindParam(":CELULAR", $CELULAR, \PDO::PARAM_STR);
            $this->db->bindParam(":EMAIL", $EMAIL, \PDO::PARAM_STR);
            $this->db->bindParam(":RUA", $RUA, \PDO::PARAM_STR);
            $this->db->bindParam(":NUMERO", $NUMERO, \PDO::PARAM_STR);
            $this->db->bindParam(":BAIRRO", $BAIRRO, \PDO::PARAM_STR);
            $this->db->bindParam(":CIDADE", $CIDADE, \PDO::PARAM_STR);
            $this->db->bindParam(":CEP", $CEP, \PDO::PARAM_STR);
            $this->db->bindParam(":ESTADO", $ESTADO, \PDO::PARAM_STR);
            $this->db->bindParam(":TIPO", $TIPO, \PDO::PARAM_STR);
            $this->db->bindParam(":SETOR", $SETOR, \PDO::PARAM_STR);
            $this->db->bindParam(":CURSO", $CURSO, \PDO::PARAM_STR);        
            $this->db->execute(); //executa a inseção de dados
            $ID_INSERT_CLIENTE = $con->lastInsertId(); 
            $return = [
                'operacao' => true,
                'id' => $ID_INSERT_CLIENTE
            ];
            return $return;
        } catch(PDOExecption $e) { 
            return $e->getMessage(); 
        } 
    }
    /**
     * Metodo de Retorno de Clientes
     */
    protected function read_cliente($CODIGO = null)
    {

        $retorno = null; //AUTO INCREMENT
        //$ID_INSERT_CLIENTE = 0;
        $con = $this->conectar();
      
        if(is_null($CODIGO))
        {
            $read = $this->db = $con->prepare("SELECT * FROM cliente"); //PREPARA SELECIONA TODOS
        }
        else
        {
            $read = $this->db = $con->prepare("SELECT * FROM cliente WHERE CODIGO = :CODIGO"); //PREPARA SELECIONA TODOS
            $read->bindParam(":CODIGO",$CODIGO,\PDO::PARAM_STR);                
        }
        $read->execute();

        $I=0;
        while($fetch = $read->fetch(\PDO::FETCH_ASSOC)){
            $retorno[$I] = [
                'ID_CLIENTE'=>$fetch['ID_CLIENTE'],
                'CODIGO'=>$fetch['CODIGO'],
                'NOME'=>$fetch['NOME'],
                'CELULAR'=>$fetch['CELULAR'],
                'EMAIL'=>$fetch['EMAIL'],
                'RUA'=>$fetch['RUA'],
                'NUMERO'=>$fetch['NUMERO'],
                'BAIRRO'=>$fetch['BAIRRO'],
                'CIDADE'=>$fetch['CIDADE'],
                'CEP'=>$fetch['CEP'],
                'ESTADO'=>$fetch['ESTADO'],
                'TIPO'=>$fetch['TIPO'],
                'SETOR'=>$fetch['SETOR'],
                'CURSO'=>$fetch['CURSO'],
            ];
            $I++;
        }
        return $retorno;
    }
    /**
     * Metodo de Update de Clientes
     */
    protected function update_cliente($ID_CLIENTE, $NOME, $CELULAR, $EMAIL, $RUA, $NUMERO, $BAIRRO, $CIDADE, $CEP, $ESTADO,  $SETOR, $CURSO)
    {
        try { 
            $this->db =  $this->conectar()->prepare("UPDATE cliente SET NOME = ? , CELULAR = ? , EMAIL = ? , RUA = ? , NUMERO =  ? , BAIRRO =  ? , CIDADE = ? , CEP = ? , ESTADO =  ? , SETOR =  ? , CURSO = ? WHERE ID_CLIENTE = ?"); //PREPARA INSERÇÃO
            $dados = array($NOME, $CELULAR, $EMAIL, $RUA, $NUMERO, $BAIRRO, $CIDADE, $CEP, $ESTADO,  $SETOR, $CURSO, $ID_CLIENTE);
            if($this->db->execute($dados)) //executa a inseção de dados
            {
                return true;
            }
            return false;
        } catch(PDOExecption $e) { 
            return $e->getMessage(); 
        } 
    }
    /**
     * Metodo de Update de Clientes
     */
    protected function delete_cliente($ID_CLIENTE)
    {
        $conn = $this->conectar();
        try { 
            $conn->beginTransaction();
            $this->db = $conn->prepare("DELETE FROM solicitacao WHERE ID_CLIENTE = ?");
            if($this->db->execute([$ID_CLIENTE])) //executa a inseção de dados
            {
                $this->db = $conn->prepare("DELETE FROM cliente WHERE ID_CLIENTE = ?");
                if($this->db->execute([$ID_CLIENTE])) //executa a inseção de dados
                {
                    $conn->commit();
                    return true;
                }

            }
            $conn->rollBack();
            return false;
            


        } catch(PDOExecption $e) { 
            $conn->rollBack();
            return $e->getMessage(); 
        } 
    }
    /*OPERAÇÕES COM SOLICITACÃO */
    /**
     * Metodo de Cadastro de Solicitações
     */
    protected function create_solicitacao($ID_CLIENTE, $SOLICITACAO)
    {
        $con = $this->conectar();
        try { 

            $this->db   = $con->prepare("INSERT INTO solicitacao (ID_CLIENTE, SOLICITACAO) VALUES (?,?)" );
            if($this->db->execute(array($ID_CLIENTE,$SOLICITACAO)))
            {
                return true;
            }
            return false;            
        } 
        catch(PDOExecption $e)
        { 
            return $e->getMessage(); 
        } 
    }
    /**
     * Metodo de Cadastro de Clientes
     */
    protected function read_solicitacao($ID_SOLICITACAO = null)
    {

        $retorno = null; //AUTO INCREMENT
        //$ID_INSERT_CLIENTE = 0;
        $con = $this->conectar();
      
        if(is_null($ID_SOLICITACAO))
        {
            $read = $this->db = $con->prepare("SELECT cliente.ID_CLIENTE, cliente.NOME, cliente.CELULAR, cliente.EMAIL, cliente.CODIGO, cliente.TIPO, solicitacao.DATA, solicitacao.ID_SOLICITACAO FROM solicitacao INNER JOIN cliente ON cliente.ID_CLIENTE = solicitacao.ID_CLIENTE"); //PREPARA SELECIONA TODOS

            $read->execute();

            $I=0;
            while($fetch = $read->fetch(\PDO::FETCH_ASSOC)){
                $retorno[$I] = [
                    'ID_CLIENTE'=>$fetch['ID_CLIENTE'],
                    'CODIGO'=>$fetch['CODIGO'],
                    'NOME'=>$fetch['NOME'],
                    'CELULAR'=>$fetch['CELULAR'],
                    'EMAIL'=>$fetch['EMAIL'],
                    'TIPO'=>$fetch['TIPO'],
                    'DATA'=>$fetch['DATA'],
                    'ID_SOLICITACAO'=>$fetch['ID_SOLICITACAO'],
                ];
                $I++;
            }
            return $retorno;

        }
        else
        {
            $read = $this->db = $con->prepare("SELECT cliente.*, solicitacao.DATA,solicitacao.SOLICITACAO, solicitacao.ID_SOLICITACAO FROM solicitacao INNER JOIN cliente ON cliente.ID_CLIENTE = solicitacao.ID_CLIENTE WHERE ID_SOLICITACAO = ?"); //PREPARA        

            $read->execute([$ID_SOLICITACAO]);

            $I=0;
            while($fetch = $read->fetch(\PDO::FETCH_ASSOC)){
                $retorno[$I] = [
                    'ID_SOLICITACAO'=>$fetch['ID_SOLICITACAO'],
                    'CODIGO'=>$fetch['CODIGO'],
                    'NOME'=>$fetch['NOME'],
                    'CELULAR'=>$fetch['CELULAR'],
                    'EMAIL'=>$fetch['EMAIL'],
                    'RUA'=>$fetch['RUA'],
                    'NUMERO'=>$fetch['NUMERO'],
                    'BAIRRO'=>$fetch['BAIRRO'],
                    'CIDADE'=>$fetch['CIDADE'],
                    'CEP'=>$fetch['CEP'],
                    'ESTADO'=>$fetch['ESTADO'],
                    'SETOR'=>$fetch['SETOR'],
                    'CURSO'=>$fetch['CURSO'],
                    'TIPO'=>$fetch['TIPO'],
                    'DATA'=>$fetch['DATA'],
                    'SOLICITACAO'=>$fetch['SOLICITACAO'],
                ];
                $I++;
            }
            return $retorno; 
        }
        
    }
    /**
     * Metodo de Update de Solicitacao
     */
    protected function update_solicitacao($ID_SOLICITACAO, $SOLICITACAO)
    {
        try { 
            $this->db =  $this->conectar()->prepare("UPDATE solicitacao SET SOLICITACAO = ? WHERE ID_SOLICITACAO = ?"); //PREPARA INSERÇÃO
            $dados = array($SOLICITACAO, $ID_SOLICITACAO);
            if($this->db->execute($dados)) //executa a inseção de dados
            {
                return true;
            }
            return false;
        } catch(PDOExecption $e) { 
            return $e->getMessage(); 
        } 
    }
    /**
     * Metodo de Update de Clientes
     */
    protected function delete_solicitacao($ID_SOLICITACAO)
    {
        $conn = $this->conectar();
        try { 
            
            $this->db = $conn->prepare("DELETE FROM solicitacao WHERE ID_SOLICITACAO = ?");
            if($this->db->execute([$ID_SOLICITACAO])) //executa a inseção de dados
            {
                return true;
            }

        } catch(PDOExecption $e) {
            return $e->getMessage(); 
        } 
    }


}

