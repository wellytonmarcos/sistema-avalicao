//Inicialização da tela
$(document).ready(function() {
	carregarLista();
});
/**
 * [carregarLista - Carrega na tela a lista de solicitações]
 */
function carregarLista()
{
	$.ajax({
		url: base_url+'administracao/listar',
		type: 'POST',
		dataType: 'html',
		data: {id: null},
	})
	.done(function(data) {
		$('#lista_solicitacoes').html(data)
	});
}
/**
 * [visualizarSolicitacao - Carrega dados em um modal e visualiza todas as informações da solicitação]
 */
function visualizarSolicitacao(ID_SOLICITACAO)
{
	$.ajax({
		url: base_url+'administracao/visualizar/'+ID_SOLICITACAO,
		type: 'POST',
		dataType: 'html',
		data: {id: ID_SOLICITACAO},
	})
	.done(function(data) {
		$('#content_visualizar').html(data);
		$('#modal_visualizar').modal();
	});
}
/**
 * [editarCliente - Carrega o modal com os dados do cliente a ser editado]
 */
function editarCliente(CODIGO)
{
	//Monta a requisição
	$.ajax({
		url: base_url + 'cadastro/buscar',
		type: 'POST',
		dataType: 'json',
		data: {
			CODIGO: CODIGO, //passa o código
		},
	})
	.done(function(data) {
		//Ao concluir, se não encontrar nada
		if(data)
		{	
			//Preenche os dados
			$('#ID_CLIENTE').val(data.ID_CLIENTE);
			$('#NOME').val(data.NOME);
			$('#EMAIL').val(data.EMAIL);
			$('#CELULAR').val(data.CELULAR);
			$('#RUA').val(data.RUA);
			$('#NUMERO').val(data.NUMERO);
			$('#BAIRRO').val(data.BAIRRO);
			$('#CIDADE').val(data.CIDADE);
			$('#CEP').val(data.CEP);
			$('#ESTADO').val(data.ESTADO);
			$('#SETOR').val(data.SETOR);
			$('#CURSO').val(data.CURSO);
			$('#modal_editar_cliente').modal();
		}
	});
}
/**
 * [enviarCadastro - Envia dados para Cadastro/Atualização]
 */
function excluirCliente()
{	
	$.ajax({
		url: base_url + 'administracao/deletar_cliente/'+$('#ID_CLIENTE').val(),
		type: 'POST',
		dataType: 'json',
		data: {		
			ID_CLIENTE: $('#ID_CLIENTE').val()
		},
	})
	.done(function(data) {
		//Se o resultado for falso, ocorreu um erro
		if(data.result)
		{
			$('#modal_editar_cliente').modal('hide');
			carregarLista();
		}
		else
		{
			alert('Falha ao Excluir!');
		}
	});
}
/**
 * [editarSolicitacao - Carrega o modal com os dados da solicitação a ser editado]
 */
function editarSolicitacao(ID_SOLICITACAO)
{
	//Monta a requisição
	$.ajax({
		url: base_url + 'administracao/buscar_solicitacao/'+ID_SOLICITACAO,
		type: 'POST',
		dataType: 'json',
		data: {
			ID_SOLICITACAO: ID_SOLICITACAO, //passa o código
		},
	})
	.done(function(data) {		
		//Preenche os dados
		$('#SOLICITACAO').html(data);
		$('#ID_SOLICITACAO').val(ID_SOLICITACAO);
		$('#modal_editar_solicitacao').modal();
		
	});
}
/**
 * [editarSolicitacao - Carrega o modal com os dados da solicitação a ser editado]
 * @param  {[string]} CODIGO [Código Alfanumérico do Cliente]
 */
function confirmaExcluirSolicitacao(ID_SOLICITACAO)
{	
	//Preenche o atributo
	$('#btn_excluir').attr('onclick','excluirSolicitacao('+ID_SOLICITACAO+')');
	$('#modal_excluir_solicitacao').modal();

}
/**
 * [editarSolicitacao - Carrega o modal com os dados da solicitação a ser editado]
 * @param  {[string]} CODIGO [Código Alfanumérico do Cliente]
 */
function excluirSolicitacao(ID_SOLICITACAO)
{
	//Monta a requisição
	$.ajax({
		url: base_url + 'administracao/deletar_solicitacao/'+ID_SOLICITACAO,
		type: 'POST',
		dataType: 'json',
		data: {
			ID_SOLICITACAO: ID_SOLICITACAO, //passa o código
		},
	})
	.done(function(data) {
		//Ao concluir, se não encontrar nada
		if(data)
		{	
			$('#modal_excluir_solicitacao').modal('hide');
			carregarLista();
		}
	});
}
/**
 * [atualizarCliente - Atualiza dados do cliente]
 */
function atualizarCliente()
{
	if(validaInput()) //Faz a validação dos campos via JS
	{
		$.ajax({
			url: base_url + 'administracao/atualizar_cliente/'+$('#ID_CLIENTE').val(),
			type: 'POST',
			dataType: 'json',
			data: {		
				NOME: $('#NOME').val(),
				EMAIL: $('#EMAIL').val(),
				CELULAR: $('#CELULAR').val(),
				RUA: $('#RUA').val(),
				NUMERO: $('#NUMERO').val(),
				BAIRRO: $('#BAIRRO').val(),
				CIDADE: $('#CIDADE').val(),
				CEP: $('#CEP').val(),
				ESTADO: $('#ESTADO').val(),
				SETOR: $('#SETOR').val(),
				CURSO: $('#CURSO').val(),
			},
		})
		.done(function(data) {
			$('#modal_editar_cliente').modal('hide');
			carregarLista();
		});
	}
}
/**
 * [atualizarSolicitacao - Atualiza solicitação do cliente]
 */
function atualizarSolicitacao()
{
	if($('#SOLICITACAO').val() != '') //Faz a validação do campos
	{
		$.ajax({
			url: base_url + 'administracao/atualizar_solicitacao/'+$('#ID_SOLICITACAO').val(),
			type: 'POST',
			dataType: 'json',
			data: {		
				SOLICITACAO: $('#SOLICITACAO').val(),
			},
		})
		.done(function(data) {
			$('#modal_editar_solicitacao').modal('hide');
			carregarLista();
		});
	}
	else
	{
		helpInputs('SOLICITACAO', 'Este campo é obrigatório');		
	}
}

/* FUNÇÕES AUXILIARES */
/**
* VALIDAÇÃO EM JAVASCRIP DOS INPUTS 
*/
function validaInput()
{
	var retorno = true; //Váriável de retorno em caso de erro

	//Nome
	if($('#NOME').val() == '')
	{
		helpInputs('NOME', 'O nome é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('NOME', '');
	}

	//E-mail
	if(!validateEmail($('#EMAIL').val()))
	{
		helpInputs('EMAIL', 'O e-mail é inválido');
		retorno = false;
	}
	else
	{
		helpInputs('EMAIL', '');
	}

	//Celular
	if($('#CELULAR').val().length != 14)
	{
		helpInputs('CELULAR', 'Celular inválido');
		retorno = false;
	}
	else
	{
		helpInputs('CELULAR', '');
	}

	//Rua
	if($('#RUA').val() == '')
	{
		helpInputs('RUA', 'Rua é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('RUA', '');
	}

	//Número
	if($('#NUMERO').val() == '')
	{
		helpInputs('NUMERO', 'Número é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('NUMERO', '');
	}

	//Bairro
	if($('#BAIRRO').val() == '')
	{
		helpInputs('BAIRRO', 'Bairro é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('BAIRRO', '');
	}

	//Cidade
	if($('#CIDADE').val() == '')
	{
		helpInputs('CIDADE', 'Cidade é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('CIDADE', '');
	}

	//Estado
	if($('#ESTADO').val() == null)
	{
		helpInputs('ESTADO', 'Estado é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('ESTADO', '');
	}

	//Setor
	if($('#SETOR').val() == '')
	{
		helpInputs('SETOR', 'Setor é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('SETOR', '');
	}

	//Curso
	if($('#CURSO').val() == '')
	{
		helpInputs('CURSO', 'Curso é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('CURSO', '');
	}

	//CEP
	if($('#CEP').val().length != 9)
	{
		helpInputs('CEP', 'CEP inválido');
		retorno = false;
	}
	else
	{
		helpInputs('CEP', '');
	}

	return retorno;
}
/**
 * [helpInputs - Define mensagem do Help]
 * @param  {string} input    [ID do Input]
 * @param  {scring} mensagem [mensagem do Help]
 */
function helpInputs(input, mensagem)
{
	$('#'+input+'_HELP').html(mensagem);
}
/**
 * [validateEmail - Valida o Email via Regex]
 * @param  {string} email    [email informado]
 */
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}