/* Inicialização do Script */
$(document).ready(function() {
	inicializaPlugins(); //chama a função de inicializar
});
/**
 * [inicializaPlugins - Inicializa os plugins da página]
 * @return {[type]} [description]
 */
function inicializaPlugins()
{
	// Aplica a mascara nos inputs
    $('#CELULAR').mask('(00)00000-0000');
    $('#CEP').mask('00000-000');
    $('#CELULAR').mask('(00)00000-0000');
}
/**
 * [defineTipo - Função chamada ao alterar input de tipo de cliente]
 */
function defineTipo()
{
	//Zera os campos
	$('#CODIGO').val('');
	$('#NOME').val('');
	$('#EMAIL').val('');
	$('#CELULAR').val('');
	$('#RUA').val('');
	$('#NUMERO').val('');
	$('#BAIRRO').val('');
	$('#CIDADE').val('');
	$('#CEP').val('');
	$('#ESTADO').val('');
	$('#SETOR').val('');
	$('#CURSO').val('');
	$('#SOLICITACAO').val('');
	//Define uma mascara alfanumérica de acordo com tipo do cliente
	if($('#TIPO').val() == 'Aluno')
	{
		$('#CODIGO').mask('AAAAAAA');
	}
	else if($('#TIPO').val() == 'Professor')
	{
		$('#CODIGO').mask('AAAA');
	}
	else if($('#TIPO').val() == 'Técnico Administrativo')
	{
		$('#CODIGO').mask('AAAAAA');
	}
	//Habilita o campo de código
	$('#CODIGO').prop('disabled', false);
	$('#CODIGO').prop('readonly', false);

}
/**
 * [verificaExiste - Função que verica se o cliente já esta cadastrado]
 */
function verificaExiste()
{
	//Se for do tipo aluno
	if($('#TIPO').val() == 'Aluno')
	{
		if($('#CODIGO').val().length == 7) //E o tamanho for 7
		{
			buscarCliente(); //Chama o buscar
		}
	}
	//Se for do tipo professor
	else if($('#TIPO').val() == 'Professor')
	{
		if($('#CODIGO').val().length == 4) //E o tamanho for 4
		{
			buscarCliente(); //Chama o buscar
		}
	}
	//Se for do tipo técnico
	else if($('#TIPO').val() == 'Técnico Administrativo')
	{
		if($('#CODIGO').val().length == 6) //E o tamanho for 6
		{
			buscarCliente(); // Chama o buscar
		}
	}
}
/**
 * [buscarCliente - Consulta se o cliente existe e preenche seus dados se existir]
 */
function buscarCliente()
{
	//Monta a requisição
	$.ajax({
		url: base_url + 'cadastro/buscar',
		type: 'POST',
		dataType: 'json',
		data: {
			CODIGO: $('#CODIGO').val(), //passa o código
		},
	})
	.done(function(data) {
		//Ao concluir, se não encontrar nada
		if(!data)
		{
			//Define o que ira fazer
			$("#OPERACAO").val('Cadastro');
			//Limpa os campos
			$('#NOME').val('');
			$('#EMAIL').val('');
			$('#CELULAR').val('');
			$('#RUA').val('');
			$('#NUMERO').val('');
			$('#BAIRRO').val('');
			$('#CIDADE').val('');
			$('#CEP').val('');
			$('#ESTADO').val('');
			$('#SETOR').val('');
			$('#CURSO').val('');
			$('#SOLICITACAO').val('');
			//Abre formuçlário
			$("#demais_campos").fadeIn();
		}
		else
		{
			//Define o que irá fazer
			$("#OPERACAO").val('Atualiza');
			//Bloqueia o código para não ser alterado
			$('#CODIGO').prop('readonly', true);
			//Preenche os dados
			$('#NOME').val(data.NOME);
			$('#EMAIL').val(data.EMAIL);
			$('#CELULAR').val(data.CELULAR);
			$('#RUA').val(data.RUA);
			$('#NUMERO').val(data.NUMERO);
			$('#BAIRRO').val(data.BAIRRO);
			$('#CIDADE').val(data.CIDADE);
			$('#CEP').val(data.CEP);
			$('#ESTADO').val(data.ESTADO);
			$('#SETOR').val(data.SETOR);
			$('#CURSO').val(data.CURSO);
			$('#SOLICITACAO').val('');
			//Abre o form
			$("#demais_campos").fadeIn();
		}
	});
}
/**
 * [enviarCadastro - Envia dados para Cadastro/Atualização]
 */
function enviarCadastro()
{
	if(validaInput()) //Faz a validação dos campos via JS
	{
		var metodo = $("#OPERACAO").val() == 'Cadastro' ? 'cadastrar' : 'atualizar'; //Define o metodo

		$.ajax({
			url: base_url + 'cadastro/'+metodo,
			type: 'POST',
			dataType: 'json',
			data: {		
				TIPO: $('#TIPO').val(),
				CODIGO: $('#CODIGO').val(),
				NOME: $('#NOME').val(),
				EMAIL: $('#EMAIL').val(),
				CELULAR: $('#CELULAR').val(),
				RUA: $('#RUA').val(),
				NUMERO: $('#NUMERO').val(),
				BAIRRO: $('#BAIRRO').val(),
				CIDADE: $('#CIDADE').val(),
				CEP: $('#CEP').val(),
				ESTADO: $('#ESTADO').val(),
				SETOR: $('#SETOR').val(),
				CURSO: $('#CURSO').val(),
				SOLICITACAO: $('#SOLICITACAO').val(),
			},
		})
		.done(function(data) {
			//Se o resultado for falso, ocorreu um erro
			if(!data.result)
			{
				//Passa pelos campos informando quais precisam de ajuste
				for(i in data.message)
				{
					helpInputs(i, data.message[i]); //Chama a função de definir Help do input
				}
			}
			else
			{
				$("#OPERACAO").val('Atualiza');
				$('#TELEFONE_CADASTRADO').html($('#CELULAR').val()); //Passa o celular para o modal
				$('#EMAIL_CADASTRADO').html($('#EMAIL').val()); //Passa o e-mail
				$('#modal_sucesso').modal(); //Abre o modal
				$('#SOLICITACAO').val(''); //limpa a solicitação, caso o cliente queira cadastrar uma nova
			}
		});
	}
	
}
/**
* VALIDAÇÃO EM JAVASCRIP DOS INPUTS 
*/
function validaInput()
{
	var retorno = true; //Váriável de retorno em caso de erro

	//Tipo
	if($('#TIPO').val() == null)
	{
		helpInputs('TIPO', 'O tipo é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('TIPO', '');
	}

	//Código
	if($('#CODIGO').val() == '')
	{
		helpInputs('CODIGO', 'O código é obrigatório');
		retorno = false;
	}
	else if($('#TIPO').val() == 'Aluno' && $('#CODIGO').val().length != 7)
	{
		helpInputs('CODIGO', 'O código do aluno deve possuir 7 carateres alfanuméricos');
		retorno = false;
	}
	else if($('#TIPO').val() == 'Professor' && $('#CODIGO').val().length != 4)
	{
		helpInputs('CODIGO', 'O código do professor deve possuir 4 carateres alfanuméricos');
		retorno = false;
	}
	else if($('#TIPO').val() == 'Técnico Administrativo' && $('#CODIGO').val().length != 6)
	{
		helpInputs('CODIGO', 'O código do técnico administrativo deve possuir 6 carateres alfanuméricos');
		retorno = false;
	}
	else
	{
		helpInputs('CODIGO', '');
	}

	//Nome
	if($('#NOME').val() == '')
	{
		helpInputs('NOME', 'O nome é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('NOME', '');
	}

	//E-mail
	if(!validateEmail($('#EMAIL').val()))
	{
		helpInputs('EMAIL', 'O e-mail é inválido');
		retorno = false;
	}
	else
	{
		helpInputs('EMAIL', '');
	}

	//Celular
	if($('#CELULAR').val().length != 14)
	{
		helpInputs('CELULAR', 'Celular inválido');
		retorno = false;
	}
	else
	{
		helpInputs('CELULAR', '');
	}

	//Rua
	if($('#RUA').val() == '')
	{
		helpInputs('RUA', 'Rua é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('RUA', '');
	}

	//Número
	if($('#NUMERO').val() == '')
	{
		helpInputs('NUMERO', 'Número é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('NUMERO', '');
	}

	//Bairro
	if($('#BAIRRO').val() == '')
	{
		helpInputs('BAIRRO', 'Bairro é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('BAIRRO', '');
	}

	//Cidade
	if($('#CIDADE').val() == '')
	{
		helpInputs('CIDADE', 'Cidade é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('CIDADE', '');
	}

	//Estado
	if($('#ESTADO').val() == null)
	{
		helpInputs('ESTADO', 'Estado é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('ESTADO', '');
	}

	//Setor
	if($('#SETOR').val() == '')
	{
		helpInputs('SETOR', 'Setor é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('SETOR', '');
	}

	//Curso
	if($('#CURSO').val() == '')
	{
		helpInputs('CURSO', 'Curso é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('CURSO', '');
	}

	//CEP
	if($('#CEP').val().length != 9)
	{
		helpInputs('CEP', 'CEP inválido');
		retorno = false;
	}
	else
	{
		helpInputs('CEP', '');
	}

	//Solicitação
	if($('#SOLICITACAO').val() == '')
	{
		helpInputs('SOLICITACAO', 'Solicitação é obrigatório');
		retorno = false;
	}
	else
	{
		helpInputs('SOLICITACAO', '');
	}
	return retorno;

}
/**
 * [helpInputs - Define mensagem do Help]
 * @param  {string} input    [ID do Input]
 * @param  {scring} mensagem [mensagem do Help]
 */
function helpInputs(input, mensagem)
{
	$('#'+input+'_HELP').html(mensagem);
}
/**
 * [validateEmail - Valida o Email via Regex]
 * @param  {string} email    [email informado]
 */
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}