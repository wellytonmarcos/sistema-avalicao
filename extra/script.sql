-- -----------------------------------------------------
-- Schema sistema
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema sistema
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sistema` DEFAULT CHARACTER SET utf8 ;
USE `sistema` ;

-- -----------------------------------------------------
-- Table `sistema`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistema`.`cliente` (
  `ID_CLIENTE` INT NOT NULL AUTO_INCREMENT,
  `CODIGO` VARCHAR(7) NOT NULL COMMENT 'CÓDIGO DO CLIENTE (ALUNO: 7 DIGITOS | PROFESSOR: 4 DIGITOS | TÉCNICO: 6 DIGITOS)',
  `NOME` VARCHAR(80) NOT NULL COMMENT 'NOME DO CLIENTE',
  `CELULAR` VARCHAR(14) NOT NULL COMMENT 'CELULAR DO CLIENTE NO FORMATO (99)99999-9999',
  `EMAIL` VARCHAR(45) NOT NULL COMMENT 'EMAIL',
  `RUA` VARCHAR(120) NOT NULL COMMENT 'RUA DO CLIENTE',
  `NUMERO` VARCHAR(4) NOT NULL COMMENT 'NÚMERO DA RUA (OU SN)',
  `BAIRRO` VARCHAR(30) NOT NULL COMMENT 'BAIRRO',
  `CIDADE` VARCHAR(30) NOT NULL COMMENT 'CIDADE DO CLIENTE',
  `CEP` VARCHAR(9) CHARACTER SET 'utf8' NOT NULL COMMENT 'CEP DO CLIENTE COM TRAÇO',
  `ESTADO` VARCHAR(2) NOT NULL COMMENT 'ESTADO DO CLIENTE (SIGLA)',
  `TIPO` VARCHAR(22) NOT NULL COMMENT 'TIPO DO CLIENTE (ALUNO, PROFESSOR, TÉCNICO ADMINISTRATIVO)',
  `SETOR` VARCHAR(30) NOT NULL COMMENT 'NOME DO SETOR',
  `CURSO` VARCHAR(45) NOT NULL COMMENT 'NOME DO CURSO',
  PRIMARY KEY (`ID_CLIENTE`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema`.`solicitacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sistema`.`solicitacao` (
  `ID_SOLICITACAO` INT NOT NULL AUTO_INCREMENT COMMENT 'ID DA SOLICITACAO',
  `ID_CLIENTE` INT NOT NULL COMMENT 'ID DO CLIENTE\n',
  `SOLICITACAO` VARCHAR(500) NOT NULL COMMENT 'SOLICITAÇÃO',
  `DATA` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'DATA DA SOLICITAÇÃO',
  PRIMARY KEY (`ID_SOLICITACAO`, `ID_CLIENTE`),
  INDEX `fk_solicitacao_cliente_idx` (`ID_CLIENTE`),
  CONSTRAINT `fk_solicitacao_cliente`
    FOREIGN KEY (`ID_CLIENTE`)
    REFERENCES `sistema`.`cliente` (`ID_CLIENTE`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

