# Sistema Avalicao

Sistema para Avaliação de Habilidades em PHP

**Estrutura do Projeto**

*  application (Pasta MVC do Sistema)
*  - controller 
*  - model
*  - view
*  config (Configurações do Sistema)
*  - config
*  public (Pasta Publica com Imagens, CSS)
*  - css
*  - img
*  - js
*  - plugins
*  - index.php
*  system (Pasta do Sistema)
*  - classes (Classes)
*  - traits (Funções Auxiliares)
*  - vendor (Composer)

**Configurações Iniciais**

Executar o código SQL contido na pasta 'extra' .

Acessar o arquivo config.php dentro da pasta config:
Caso a aplicação esteja dentro de alguma pasta, adicione este caminho a variável
`$project_folder`, ex: C:\wamp64\www\meusistema, `$project_folder` = 'meusistema';
A variável DESTINATARIO_DEFAULT deverá ser setada com o e-mail de quem receberá as solicitações.
