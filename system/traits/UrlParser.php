<?php
namespace Src\Traits;

trait UrlParser{

    /**
	 * [Retorna um Array da URL dividida por "/"]
	 * @return [array]           [URL]
	 */
    public function parse_url()
    {
        return explode("/",rtrim($_GET['url']),FILTER_SANITIZE_URL); //Retorna Array
    }
}