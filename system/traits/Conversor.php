<?php
namespace Src\Traits;

trait Conversor{

    /**
	 * [Converte a data hora do formato SQL para o formato brasileiro (aaaa-mm-dd H:m:s para dd/mm/aaaa H:m:s)]
	 * @param  [string] $data_sql [Data no formato aaaa-mm-dd H:m:s]
	 * @return [string]           [Data no formato dd/mm/aaaa H:m:s]
	 */
    public function converter_datahora_brasil($data_sql) {
    
	    if ($data_sql != null){ // se for diferente de null
	        
	        $data_hora = explode(' ', $data_sql); // armazena em um vetor as posições data(0) e hora(1)
	        $data = explode('-', $data_hora[0]); // armazena em outro vetor o dia, mes e ano
	        $data_br = ($data[2] . '/' . $data[1] . '/' . $data[0] . ' ' . $data_hora[1]); // concatena as 4 partes colocando uma / entre cada parte
	        return $data_br; // retorna a data formatada (formato dd/mm/aaaa H:m:s)
	    }
	    return 'Não Encontrada'; // mensagem
	}
}