<?php	
namespace Src\Classes;

/**
 * Classe responsável renderizar as views
 */
class Render 
{
	private $dir; //Diretório da View
	private $title; //Titulo da Página

	public function get_dir() { return $this->dir; } //Retorna o diretório
	public function set_dir($dir) { $this->dir = $dir; } //Seta o diretório
	public function get_title() { return $this->title; } //Retorna o Titulo
	public function set_title($title) { $this->title = $title; } //Seta p titulo
    
    /**
     * [Renderiza a view modelo]
     * @return [view]                [include na view]
     */
    public function render_layout()
	{
		 include_once(BASE_ABS.'application/view/Layout.php');
	}
	/**
     * [Renderiza o Head personalizado da view]
     * @return [view]                [include no Head]
     */
	public function add_head()
	{
		//Inclui a view, se encontrar
		if(file_exists(BASE_ABS."application/view/{$this->get_dir()}/head.php"))
		{
	        include(BASE_ABS."application/view/{$this->get_dir()}/head.php");
	    }
	}
	/**
     * [Renderiza o Main da view]
     * @return [view]                [include no Main]
     */
	public function add_main()
	{
		//Inclui a view, se encontrar
		if(file_exists(BASE_ABS."application/view/{$this->get_dir()}/main.php")){
	        include(BASE_ABS."application/view/{$this->get_dir()}/main.php");
	    }
	}
	/**
     * [Renderiza o Modal personalizado da view]
     * @return [view]                [include no Modal]
     */
	public function add_modal()
	{
		//Inclui a view, se encontrar
		if(file_exists(BASE_ABS."application/view/{$this->get_dir()}/modal.php")){
	        include(BASE_ABS."application/view/{$this->get_dir()}/modal.php");
	    }
	}


}
