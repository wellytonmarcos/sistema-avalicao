<?php
namespace Src\Classes;

use Src\Traits\UrlParser;
/**
 * Classe responsável traduzir a URL para o Controller
 */
class Routes{

    use UrlParser;

    private $route; //Array com posições de rota

    /**
     * [Retorna o Controller responsável pela Rota]
     * @return [string]                [Controller]
     */
    public function get_route(){

        $url = $this->parse_url(); // Obtem rota
        $ini = $url[0]; //Pega 1º posição

        $this->route = $this->_translact_route(); //Obtem array de tradução

        if(array_key_exists($ini, $this->route)) //Verifica existe indice
        {
            if(file_exists("../application/controller/{$this->route[$ini]}.php")) //Se encontrar
            {
                return $this->route[$ini]; //Retorna Controller
            }
            else
            {
                return "Home"; //Arquivo nao encontrado encontrado na pasta, redireciona para Main
            }
        }
        else
        {
            return "Erro404"; //Redireciona 404
        }
    }
   /**
     * [Retorna o array que traduz a URL]
     * @return [array]                [Lista de Controllers]
     */
    private function _translact_route()
    {
        return [
            '' => 'Home',
            'cadastro' => 'Cadastro',
            'administracao' => 'Administracao',

        ];
    }
}
